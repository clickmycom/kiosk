
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>A4</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Sarabun&display=swap" rel="stylesheet">
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body onload="window.print()">


  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

<?php
  foreach ($data as $key => $value) {
    if($key>=1){
      echo "<br>";
    }
?>
    <section class="sheet padding-10mm" style="width: 80mm;font-family: 'Sarabun', sans-serif;padding-top: 1px;padding-bottom: 1px;">
       <div class="row">
          

        @if(!empty($value->nexttime))
        <div class="col-sm-12" style="text-align: center;">

            <?php
                $nexttime = explode(":",$value->nexttime);
                ?>
                <h3 style="margin-top: 0px;">นัด {{ $nexttime[0] }}:{{ $nexttime[1] }} น.</h3>
        </div>
        @endif
   

          <div class="col-sm-12" style="text-align: center;">
             <h3 style="margin-top: 0px;">{{ $value->department }}</h3>
          </div>
          <div class="col-sm-12" style="text-align: center;" >
             <h2 style="margin-top: 2px;">{{ $value->HN }}</h2>
             <h6 style="margin-top: 2px;">{{ $value->vn }}</h6>
          </div>
          <div class="col-sm-12" style="text-align: center;" >
             <h4 style="margin-top: 2px;">{{ $value->fullname }}</h4>
          </div>
          <div class="col-sm-12" style="text-align: center;" >
            <h5 style="margin-top: 2px;">อายุ {{ $value->date_thai }}</h5>
          </div>
          <div class="col-sm-12" style="text-align: center;" >
             <h4 style="margin-top: 2px;">{{ $value->pttypename }}</h4>
          </div>
          <div class="col-sm-12" style="text-align: center;" >
             <h1 style="margin-top: 2px;font-size: 55px;">{{ $value->oqueue }}</h1>
          </div>
          <div class="col-sm-12">
             <h5 style="margin-top: 2px;text-align: left;">
                {{ $value->vstdate_text }}
                <div style="float: right;">เวลา {{ $value->vsttime }}</div>
             </h5>
          </div>
       </div>
       <div class="row">
          <div class="col-sm-6" style="text-align: center;">
             <img  style="width: 130px;height: 30px;float: left;" src="data:image/png;base64,{{DNS1D::getBarcodePNG($value->hn, 'C128A')}}" alt="barcode" />
             <p style="font-size: 9px; margin-left: 28px; position: absolute;margin-top: 34px;margin-left: 49px;">{{ $value->hn }}</p>
          </div>
          <div class="col-sm-6" >
             <img style="width: 83px;float: right;" src="data:image/png;base64,{{DNS2D::getBarcodePNG($value->qr, 'QRcode')}}" alt="barcode" />
          </div>
          <p style="text-align: center;margin-top: 15px;"><br><br><br><br>Scan QR Code ที่นี่ เพื่อทราบคิวของคุณ</p>
          <p style="font-weight: 800;">&nbsp;&nbsp;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>
          <p style="width: 100%;border-bottom: 1px solid #000;">&nbsp;&nbsp;Officer:</p>
          <p style="width: 100%;border-bottom: 1px solid #000;">&nbsp;&nbsp;แพทย์:</p>
          <p style="width: 100%;border-bottom: 1px solid #000;">&nbsp;&nbsp;ห้องยา:</p>
          <p style="width: 100%;border-bottom: 1px solid #000;">&nbsp;&nbsp;การเงิน:</p>
       </div>
    </section>

<?php
  }
?>

  <script type="text/javascript">
    
    window.onafterprint = function(){
//       window.history.back();
        
        window.location="/?success";
        
        
    }
  
  </script>

</body>

</html>
