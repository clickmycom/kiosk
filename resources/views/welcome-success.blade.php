<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>


    <script src="{{ asset('js/jquery.cookie.js') }}"></script>

<?php
    date_default_timezone_set("Asia/Bangkok");
    ?>
<script type="text/javascript">



  $(document).ready(function() {
                        var momentNow = moment();
                          momentNow.locale('th');
    var interval = setInterval(function() {
                               
        
                       var christianYear = momentNow.format('YYYY');
                       var buddhishYear = " พ.ศ."+(parseInt(christianYear) + 543).toString();
                              
                        var date_thai = momentNow
                        .format(momentNow.format('LL', buddhishYear).replace('YY', buddhishYear.substring(2, 4)))
                               .replace(christianYear, buddhishYear);
                               

                               $('#time-part2').html('วัน'+momentNow.format('dddd')+'ที่ '+date_thai);
                               
                               
    }, 90);
    
    $('#stop-interval').on('click', function() {
        clearInterval(interval);
    });
                    
                    
                    
    var i22 = 0;
    var myVar = '';
                    
    $("input#showdata").keypress(function(){
                                 
                                 clearTimeout(myVar);
                                 
                                 if(i22 < jQuery("#showdata").val().length){
                                    
                                    myVar = setTimeout(function(){
                                              $('#myForm').trigger('submit');
                                    }, 4000);
                                    
                                    i22 = jQuery("#showdata").val().length;
                                    
                                 }
                                 
                                 
                                 
                                 
//             myVar = setTimeout(function(){ $('#myForm').trigger('submit'); }, 3000);
//
//                                 i22 += 1;
//
//                                 if(i22>=1){
//
//                                        clearTimeout(myVar);
//
//                                        setTimeout(function(){
//
//                                                  $('#myForm').trigger('submit');
//
//                                        }, 4000);
//                                 }
                                 
                                 
                                 
                                 
                                 
      
    });
});

</script>


<script type="text/javascript">
$(document).ready(function() {
         
           setInterval(function(){
                  
                  $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}'
                        }
                  });

                  $.ajax({
                        type: 'GET',
                        url: "{{ route('get_time') }}",
                    
                        success: function (data) {
                          $("#time-part").html(data);
                        },
                        error: function (data) {
                              console.log('error');
                        }
                  }); //end ajax send
                  
          },100);
                  
                  
});

</script>


        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }


.hideMe {
                -moz-animation: cssAnimation 0s ease-in 5s forwards;
                /* Firefox */
                -webkit-animation: cssAnimation 0s ease-in 5s forwards;
                /* Safari and Chrome */
                -o-animation: cssAnimation 0s ease-in 5s forwards;
                /* Opera */
                animation: cssAnimation 0s ease-in 5s forwards;
                -webkit-animation-fill-mode: forwards;
                animation-fill-mode: forwards;
            }
            @keyframes cssAnimation {
                to {
                    width:0;
                    height:0;
                    overflow:hidden;
                    margin-top:0px;
                    display:none !important;
                    padding: 0px !important;
                }
                .alert{
                    padding: 0px !important;
                }
            }
            


        @-webkit-keyframes cssAnimation {
                to {
                    width:0;
                    height:0;
                    visibility:hidden;
                    margin-top:0px;
                    display:none !important;
                    padding: 0px !important;
                }
            .alert{
                padding: 0px !important;
            }
            }


    #fade-in {
      height: 150px;
      width: 1px;
      opacity: 0;
      transition: all .75s ease;
    }

    #fade-in.show {
      opacity: 1;
      height: 150px;
      width: 500px;
    }
        </style>

        <script>
$(function(){
    var wat = 1;
    setInterval(function(){
        $("#btnGetJson").ready(function() {
            var   xmlhttp, myObj, x, txt;
            
            xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                    
                    var txt = this.responseText;
                    var obj = JSON.parse(txt);
                    
                    //ถ้าอ่านข้อมูลจากบัตรประชาชนได้
                    if(txt == "{}" && $.cookie('smartcard_input') == 'yes'){
                            jQuery(".alert.alert-danger").hide();
                               
                               if(jQuery("div.hideMe").hasClass("hideMe") == 'false'){
                                    jQuery("div.hideMe").addClass("hideMe");
                               }
                               
                    }
                               
                    if($.cookie('c_id') != obj.cid && obj.cid != null){
                               
                         $.cookie('smartcard_input','yes');
                         $.cookie('c_id',obj.cid);
                         document.getElementById("showdata").value = obj.cid;
                         jQuery('button[type="submit"]').trigger('click');
                               
                             if(jQuery("div.hideMe").hasClass("hideMe") == 'true'){
                               console.log('มี');
                                  jQuery("div.hideMe").removeClass("hideMe");
                             }
                            
                    }
                    
                    if(obj.cid == null){
                        $.cookie('c_id',null);
                               $.cookie('smartcard_input','no');
                               console.log('ไม่เสียบ');
                               
                               
                    }
                               
                    
               }
            };
                               
            xmlhttp.open("POST", "https://localhost:8443/smartcard/data/", true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.send();
                               
        });
    },500);
});

</script>

    </head>
    <body style="background-image: url('{{ asset('6294.jpg') }}');    background-size: cover;background-position: bottom;">

        
        <div class="container" style="background-color: rgba(0, 0, 0, 0.6);background: rgba(0, 0, 0, 0.6);">
                <div class="content">
                    <div class="title">
                        <form method="POST" id="myForm" action="{{ route('home.store') }}" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                            <div class="form-group">
                                

                               
                                <div class="alert hideMe"  style="margin-top:10px;">
                                      <div class="alert-success">
                                            {กรุณารับบัตรคิว
                                      </div>
                                </div>

<div id='time-part' style="font-size: 180px;color:#fff;    font-weight: 700;font-family: sans-serif;"></div>
<div id='time-part2' style="font-size: 40px;color:#fff;    font-weight: 700;font-family: sans-serif;"></div>
    <h1 style="font-size:1em; color: #fff; font-weight: 700;margin-bottom: -40px;">จุดลงทะเบียนผู้ป่วยนัด</h1>
                            <span style="font-size: 30px; color: #fff; font-weight: 700;">ระบุเลข HN,VN หรือ หมายเลขบัตรประชาชน</span>
                              <input id="showdata" type="text" class="form-control"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"     name="hn" autofocus style="    font-weight: bold;height: 130px;font-size: 50px;text-align: center;width: 100%;background: transparent;border: none;border-radius: 0;box-shadow: inherit;padding-right: 40px;color: #fff;border: 1px solid #ccc;"   autocomplete="off" maxlength="13" >
                            </div>
                         <button  style="display: none;" type="submit" class="btn green m-t-xs bottom15-xs"><i class="fa fa-save"></i>&nbsp;บันทึกข้อมูล</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>


    </body>


    <script type="text/javascript">
       
        if($("input[name='hn']").val() != ''){
           location.reload();
        }
        
    </script>


</html>
