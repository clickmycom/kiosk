<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use \Milon\Barcode\DNS1D;
use DNS2D;
use Config;
use Response;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
     
        return view('welcome');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('welcome-error');
    }

    public function get_id_card(){

        

        echo "sss";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        if(empty($input)){
            abort(404);
        }
        
        
        $request->session()->pull('error',null);
        $input = $request->all();
        
        $size = strlen($input['hn']);
        
       
        if($size==1){
            $input['hn'] = '000000'.$input['hn'];
        }
        
        if($size==2){
            $input['hn'] = '00000'.$input['hn'];
        }
        
        if($size==3){
            $input['hn'] = '0000'.$input['hn'];
        }
        
        if($size==4){
            $input['hn'] = '000'.$input['hn'];
        }
        
        if($size==5){
            $input['hn'] = '00'.$input['hn'];
        }
        if($size==6){
            $input['hn'] = '0'.$input['hn'];
        }
        
        $size = strlen($input['hn']);
        
        switch ($size) {
                
                
                   
                
//            case $size==1:
//                  $type_input = 'oqueue';
//                  $cards =    DB::select('select "INREGION" as inregion,
//                              "FULL AGE" as fullage,
//                              ov.* ,pt.*,
//                              concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
//                              pty.name as pttypename,
//                              kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
//                              from ovst ov
//                              left outer join patient pt on pt.hn=ov.hn
//                              left outer join pttype pty on pty.pttype=ov.pttype
//                              left outer join kskdepartment kp on kp.depcode=ov.main_dep
//                              where vstdate=CURDATE() and ov.oqueue="'.$input['hn'].'"');
//
//
//
//                  if(!empty($cards)){
//
//                      $visit_no = $cards[0]->vn;
//                      $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('oapp.hn',$cards[0]->hn)->where('oapp.nextdate',$cards[0]->vstdate)->update([
//                          'opd_queue_slot_id' => 1
//                      ]);
//
//
//                      if(sizeof($cards)==1){
//                          $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
//                          $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
//
//                      }else{
//                          foreach ($cards as $key => $value) {
//                              $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
//                              $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
//                          }
//                      }
//
//
////                      if( config('log_input.input') == $input['hn']){
////                          $ii = config('log_input.total');
////                          $ii+=1;
////                          Config::write(['log_input.total' => $ii]);
////
////                          if($ii %  (sizeof($cards)+1) == 0){
////                               return view('view')->with([
////                                  'data' => $cards
////                              ]);
////                          }
////
////                          return redirect()->route('home.create');
////                      }else{
////                          Config::write(['log_input.input' => $input['hn']]);
////                          Config::write(['log_input.total' => 0]);
//
//                          $request->session()->flash('success', 'กรุณารับบัตรคิว');
//
//                          return view('view')->with([
//                              'data' => $cards
//                          ]);
//
////                      }
//
//
//                  }else{
//
//                      $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
//
//
//                      return redirect()->route('home.create');
//                  }
//                  break;
//
//            case $size==2:
//                  $type_input = 'oqueue';
//                  $cards =    DB::select('select "INREGION" as inregion,
//                              "FULL AGE" as fullage,
//                              ov.* ,pt.*,
//                              concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
//                              pty.name as pttypename,
//                              kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
//                              from ovst ov
//                              left outer join patient pt on pt.hn=ov.hn
//                              left outer join pttype pty on pty.pttype=ov.pttype
//                              left outer join kskdepartment kp on kp.depcode=ov.main_dep
//                              where vstdate=CURDATE() and ov.oqueue="'.$input['hn'].'"');
//
//
//
//                  if(!empty($cards)){
//
//                      $visit_no = $cards[0]->vn;
//                      $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('oapp.hn',$cards[0]->hn)->where('oapp.nextdate',$cards[0]->vstdate)->update([
//                          'opd_queue_slot_id' => 1
//                      ]);
//
//
//                      if(sizeof($cards)==1){
//                          $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
//                          $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
//                      }else{
//                          foreach ($cards as $key => $value) {
//                              $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
//                              $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
//                          }
//                      }
//
//
////                      if( config('log_input.input') == $input['hn']){
////                          $ii = config('log_input.total');
////                          $ii+=1;
////                          Config::write(['log_input.total' => $ii]);
////
////                          if($ii %  (sizeof($cards)+1) == 0){
////                               return view('view')->with([
////                                  'data' => $cards
////                              ]);
////                          }
////
////                          return redirect()->route('home.create');
////                      }else{
////                          Config::write(['log_input.input' => $input['hn']]);
////                          Config::write(['log_input.total' => 0]);
//
//
//                                         $request->session()->flash('success', 'กรุณารับบัตรคิว');
//                          return view('view')->with([
//                              'data' => $cards
//                          ]);
//
////                      }
//
//
//                  }else{
//
//                      $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
//                      return redirect()->route('home.create');
//                  }
//                  break;
//
//            case $size==3:
//                  $type_input = 'oqueue';
//                  $cards =    DB::select('select "INREGION" as inregion,
//                              "FULL AGE" as fullage,
//                              ov.* ,pt.*,
//                              concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
//                              pty.name as pttypename,
//                              kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
//                              from ovst ov
//                              left outer join patient pt on pt.hn=ov.hn
//                              left outer join pttype pty on pty.pttype=ov.pttype
//                              left outer join kskdepartment kp on kp.depcode=ov.main_dep
//                              where vstdate=CURDATE() and ov.oqueue="'.$input['hn'].'"');
//
//
//
//
//                  if(!empty($cards)){
//
//                      $visit_no = $cards[0]->vn;
//                      $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('oapp.hn',$cards[0]->hn)->where('oapp.nextdate',$cards[0]->vstdate)->update([
//                          'opd_queue_slot_id' => 1
//                      ]);
//
//
////                                         echo "<pre>";
////                                                                                 print_r($xx);
////                                                                                 echo "</pre>";
////                                                                                 exit;
//
//                      if(sizeof($cards)==1){
//                          $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
//                          $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
//                      }else{
//                          foreach ($cards as $key => $value) {
//                              $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
//                              $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
//                          }
//                      }
//
//
////                      if( config('log_input.input') == $input['hn']){
////                          $ii = config('log_input.total');
////                          $ii+=1;
////                          Config::write(['log_input.total' => $ii]);
////
////
////
////
////                          if($ii %  (sizeof($cards)+1) == 0){
////                               return view('view')->with([
////                                  'data' => $cards
////                              ]);
////                          }
////
////                          return redirect()->route('home.create');
////                      }else{
////                          Config::write(['log_input.input' => $input['hn']]);
////                          Config::write(['log_input.total' => 0]);
//                          $request->session()->flash('success', 'กรุณารับบัตรคิว');
//
//                          return view('view')->with([
//                              'data' => $cards
//                          ]);
//
////                      }
//
//
//                  }else{
//
//                      $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
//                      return redirect()->route('home.create');
//                  }
//                  break;
//
//
//    case $size==4:
//          $type_input = 'oqueue';
//          $cards =    DB::select('select "INREGION" as inregion,
//                      "FULL AGE" as fullage,
//                      ov.* ,pt.*,
//                      concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
//                      pty.name as pttypename,
//                      kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
//                      from ovst ov
//                      left outer join patient pt on pt.hn=ov.hn
//                      left outer join pttype pty on pty.pttype=ov.pttype
//                      left outer join kskdepartment kp on kp.depcode=ov.main_dep
//                      where vstdate=CURDATE() and ov.oqueue="'.$input['hn'].'"');
//
//
//
//          if(!empty($cards)){
//
//              $visit_no = $cards[0]->vn;
//              $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('oapp.hn',$cards[0]->hn)->where('oapp.nextdate',$cards[0]->vstdate)->update([
//                  'opd_queue_slot_id' => 1
//              ]);
//
//
//              if(sizeof($cards)==1){
//                  $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
//                  $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
//              }else{
//                  foreach ($cards as $key => $value) {
//                      $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
//                      $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
//                  }
//              }
//
//
////              if( config('log_input.input') == $input['hn']){
////                  $ii = config('log_input.total');
////                  $ii+=1;
////                  Config::write(['log_input.total' => $ii]);
////
////                  if($ii %  (sizeof($cards)+1) == 0){
////                       return view('view')->with([
////                          'data' => $cards
////                      ]);
////                  }
////
////                  return redirect()->route('home.create');
////              }else{
////                  Config::write(['log_input.input' => $input['hn']]);
////                  Config::write(['log_input.total' => 0]);
//
//                  $request->session()->flash('success', 'กรุณารับบัตรคิว');
//                  return view('view')->with([
//                      'data' => $cards
//                  ]);
//
////              }
//
//
//          }else{
//
//              $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
//              return redirect()->route('home.create');
//          }
//          break;
                      
                
case $size==6:
                
            
           
                          $type_input = 'oqueue';
                
                          $cards = $this->get_db($input['hn'],'hn');
                



                          if(!empty($cards)){
                
                              // $visit_no = $cards[0]->vn;
                              // $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('oapp.hn',$cards[0]->hn)->where('oapp.nextdate',$cards[0]->vstdate)->update([
                              //     'opd_queue_slot_id' => 1
                              // ]);
                
                
                              // if(sizeof($cards)==1){
                              //     $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
                              //     $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
                              // }else{
                              //     foreach ($cards as $key => $value) {
                              //         $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
                              //         $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
                              //     }
                              // }
                              
                
                //              if( config('log_input.input') == $input['hn']){
                //                  $ii = config('log_input.total');
                //                  $ii+=1;
                //                  Config::write(['log_input.total' => $ii]);
                //
                //                  if($ii %  (sizeof($cards)+1) == 0){
                //                       return view('view')->with([
                //                          'data' => $cards
                //                      ]);
                //                  }
                //
                //                  return redirect()->route('home.create');
                //              }else{
                //                  Config::write(['log_input.input' => $input['hn']]);
                //                  Config::write(['log_input.total' => 0]);
                
                                  if($cards != 'flase'){
                                      $request->session()->flash('success', 'กรุณารับบัตรคิว');
                                      
                                      
                                      return view('view')->with([
                                          'data' => $cards
                                      ]);
                                  }else{
                                      $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                                      return redirect()->route('home.create');
                                  }
                
                //              }
                
                
                          }else{
                
                              $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                              return redirect()->route('home.create');
                          }
                          break;
                                                 
            case $size==7:
                $type_input = 'hn';
                $cards = $this->get_db($input['hn'],'hn');


              
            
                if(!empty($cards)){

                    // $visit_no = $cards[0]->vn;
                    // $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('visit_vn',$visit_no)->where('visit_no',null)->update([
                    //     'opd_queue_slot_id' => 1
                    // ]);

                    // if(sizeof($cards)==1){
                    //     $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
                    //     $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
                    // }else{
                    //     foreach ($cards as $key => $value) {
                    //         $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
                    //         $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
                    //     }
                    // }


//                    if( config('log_input.input') == $input['hn']){
//                        $ii = config('log_input.total');
//                        $ii+=1;
//                        Config::write(['log_input.total' => $ii]);
//
//                        if($ii %  (sizeof($cards)+1) == 0){
//                             return view('view')->with([
//                                'data' => $cards
//                            ]);
//                        }
//
//                        return redirect()->route('home.create');
//                    }else{
//                        Config::write(['log_input.input' => $input['hn']]);
//                        Config::write(['log_input.total' => 0]);
                        if($cards != 'flase'){
                            $request->session()->flash('success', 'กรุณารับบัตรคิว');
                            
                            
                            return view('view')->with([
                                'data' => $cards,
                            ]);
                        }else{
                            $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                            return redirect()->route('home.create');
                        }

//                    }


                }else{
                    
                    $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                    return redirect()->route('home.create');
                }
                break;

            case $size==12:
                $type_input = 'vn';
                $cards = $this->get_db($input['hn'],'vn');
//                $cards =    DB::select('select "INREGION" as inregion,
//                            "FULL AGE" as fullage,
//                            ov.* ,pt.*,
//                            concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
//                            pty.name as pttypename,
//                            kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
//                            from ovst ov
//                            left outer join patient pt on pt.hn=ov.hn
//                            left outer join oapp a on ov.hn = a.hn and ov.vstdate = a.nextdate
//                            left outer join pttype pty on pty.pttype=ov.pttype
//                            left outer join kskdepartment kp on kp.depcode=ov.main_dep
//                            where vstdate=CURDATE() and ov.vn="'.$input['hn'].'"');
                
                // echo "<pre>";
                // print_r($cards);
                // echo "</pre>";
                // exit;
                if(!empty($cards)){

                    // $request->session()->flash('success', 'กรุณารับบัตรคิว');
                    // $visit_no = $cards[0]->vn;
                    // $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('visit_vn',$visit_no)->where('visit_no',null)->update([
                    //     'opd_queue_slot_id' => 1
                    // ]);
                                       
                    // $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
                    // $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
                    if($cards != 'flase'){
                        $request->session()->flash('success', 'กรุณารับบัตรคิว');
                        
                        
                        return view('view')->with([
                            'data' => $cards
                        ]);
                    }else{
                        $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                        return redirect()->route('home.create');
                    }
                    
                }else{
                    $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                    return redirect()->route('home.create');
                }
                break;


            case $size == 13:
                $type_input = 'hn';
                                       
                $cards = $this->get_db_cid($input['hn'],'cid');
                
                // $cards =    DB::select('select a.nexttime,"INREGION" as inregion,
                //             "FULL AGE" as fullage,
                //             ov.* ,pt.*,
                //             concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
                //             pty.name as pttypename,
                //             kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
                //             from ovst ov
                //             left outer join patient pt on pt.hn=ov.hn
                //             left outer join oapp a on ov.hn = a.hn and ov.vstdate = a.nextdate
                //             left outer join pttype pty on pty.pttype=ov.pttype
                //             left outer join kskdepartment kp on kp.depcode=ov.main_dep
                //             where vstdate=CURDATE() and pt.cid="'.$input['hn'].'"');
           
                if(!empty($cards)){


                    // $visit_no = $cards[0]->vn;
                    // $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('visit_vn',$visit_no)->where('visit_no',null)->update([
                    //     'opd_queue_slot_id' => 1
                    // ]);

                    // if(sizeof($cards)==1){
                    //     $cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
                    //     $cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
                    // }else{
                    //     foreach ($cards as $key => $value) {
                    //         $cards[$key]->vstdate_text =  $this->DateThai($cards[$key]->vstdate);
                    //         $cards[$key]->date_thai = $this->birthday_thai($cards[0]->birthday);
                    //     }
                    // }



//                    if( config('log_input.input') == $cards[0]->hn){
//                        $ii = config('log_input.total');
//                        $ii+=1;
//                        Config::write(['log_input.total' => $ii]);
//
//                        if($ii %  (sizeof($cards)+1) == 0){
//                             return view('view')->with([
//                                'data' => $cards
//                            ]);
//                        }
//
//                        return redirect()->route('home.create');
//                    }else{
//                        Config::write(['log_input.input' => $cards[0]->hn]);
//                        Config::write(['log_input.total' => 0]);
                    
                 
                    if($cards != 'flase'){
                        $request->session()->flash('success', 'กรุณารับบัตรคิว');
                        
                        
                        return view('view')->with([
                            'data' => $cards
                        ]);
                    }else{
                        $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                        return redirect()->route('home.create');
                    }
                                  
                    
                }else{
                    $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                    return redirect()->route('home.create');
                }
                break;
            
            default:
             
                return view('welcome');
                break;
        }

        // $date_start = Carbon::parse($cards[0]->vstdate, 'UTC');

        // //$text = $date->isoFormat('MMMM Do YYYY, h:mm:ss a').' ถึง '.$query->coupon_rental_end_date;
        // $text = $date_start->isoFormat(' Do MMMM YYYY').' น.';

        // echo $text;
        // exit;

        //$cards[0]->vstdate_text =  $this->DateThai($cards[0]->vstdate);
        //$cards[0]->date_thai = $this->birthday_thai($cards[0]->birthday);
        
        // echo "<pre>";
        // print_r($cards);
        // echo "</pre>";
        // exit;
         // return view('view')->with([
         //    'data' => $cards[0]
         // ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
        $strMonthThai=$strMonthCut[$strMonth];
        return "วันที่ $strDay $strMonthThai $strYear";
    }
                                       
                                       
                                       
   public function get_time(){
        date_default_timezone_set("Asia/Bangkok");
        $time = date("H:i:s");
       
                                       return $time;
//        return Response::json($data);
   }


   public function birthday_thai($strDate){

        $text_date = '';
        $birthday = $strDate;
        $today = date("Y-m-d");
            

        list($byear, $bmonth, $bday)= explode("-",$birthday);
        list($tyear, $tmonth, $tday)= explode("-",$today);
            
        $mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear);
        $mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
        $mage = ($mnow - $mbirthday);
        
        
        $u_y=date("Y", $mage)-1970;
        $u_m=date("m",$mage)-1;
        $u_d=date("d",$mage)-1;

        if($u_y !=0){
            $text_date.=$u_y." ปี ";
        }

        if($u_m !=0){
            $text_date.=$u_m." เดือน ";
        }

        if($u_d !=0){
            $text_date.=$u_d." วัน";
        }

        return $text_date;

    }

                                       
   public function get_db($data_input,$type){
      
           $cards =    DB::select('select a.nexttime,"INREGION" as inregion,
           "FULL AGE" as fullage,
           ov.* ,pt.*,
           concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
           pty.name as pttypename,
           kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
           from ovst ov
           left outer join patient pt on pt.hn=ov.hn
           left outer join oapp a on ov.hn = a.hn and ov.vstdate = a.nextdate
           left outer join pttype pty on pty.pttype=ov.pttype
           left outer join kskdepartment kp on kp.depcode=ov.main_dep
           where ov.vstdate=CURDATE() and ov.'.$type.'="'.$data_input.'"');


           
                                 // echo "<pre>";
                                 // print_r($cards);
                                 // echo "</pre>";
                                 // exit;

            if(empty($cards)){
                                  return 'flase';
           }else{
                                    foreach ($cards as $key => $value) {
                                  
                                  if(empty($value->vn)){
                                                                         $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                                                                         return redirect()->route('home.create');
                                                                   
                                                                   }else{
                                  
                                      
                                        $data[$value->vn] = $value;
                                  

                                           $visit_no = $data[$value->vn]->vn;
                                                      
                                                              
//                                           $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('visit_vn',$visit_no)->where('visit_no',null)->update([
//                                               'opd_queue_slot_id' => 1
//                                           ]);
                                            
                                            $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('hn',$value->hn)->where('nextdate',$value->vstdate)->update([
                                                                                          'opd_queue_slot_id' => 1
                                                                                      ]);
                                  
                                           if(sizeof($cards)==1){
                                               $data[$value->vn]->vstdate_text =  $this->DateThai($data[$value->vn]->vstdate);
                                               $data[$value->vn]->date_thai = $this->birthday_thai($data[$value->vn]->birthday);
                                           }else{
                                                               
                                               $data[$value->vn]->vstdate_text =  $this->DateThai($data[$value->vn]->vstdate);
                                              $data[$value->vn]->date_thai =  $this->birthday_thai($data[$value->vn]->birthday);
                                               
                                               
                                           }

                                         }
                                         
                                     
                                        
                                        
                                  }
           return $data;
          }
           
                                  
                                  
      

   }
                                  
                                  
   public function get_db_cid($data_input,$type){
           $cards =    DB::select('select a.nexttime,"INREGION" as inregion,
           "FULL AGE" as fullage,
           ov.* ,pt.*,
           concat(pt.pname,pt.fname,"  ",pt.lname) as fullname,concat(left( ov.hn,4),"-",RIGHT( ov.hn,3)) as HN,
           pty.name as pttypename,
           kp.department,if(year(curdate())-year(pt.birthday) >69," ¤ÔÇ 70 »Õ","¤ÔÇ"),concat("http://www.bph.moph.go.th/quetestqr?vn=",ov.vn) as qr
           from ovst ov
           left outer join patient pt on pt.hn=ov.hn
           left outer join oapp a on ov.hn = a.hn and ov.vstdate = a.nextdate
           left outer join pttype pty on pty.pttype=ov.pttype
           left outer join kskdepartment kp on kp.depcode=ov.main_dep
           where ov.vstdate=CURDATE() and pt.'.$type.'="'.$data_input.'"');

                               
       
                                  
          if(empty($cards)){
   
              return 'flase';
          }else{
           
                foreach ($cards as $key => $value) {
                                  
                               
                          if(empty($value->vn)){
                                $request->session()->flash('error', 'กรุณาติดต่อห้องบัตร');
                                return redirect()->route('home.create');
                          }else{
                            $data[$value->vn] = $value;
                           
                               $visit_no = $data[$value->vn]->vn;
                               $xx = DB::table('oapp')->select('hn','visit_vn','visit_no','opd_queue_slot_id')->where('visit_vn',$visit_no)->where('visit_no',null)->update([
                                   'opd_queue_slot_id' => 1
                               ]);

                                if(sizeof($cards)==1){
                                                                             $data[$value->vn]->vstdate_text =  $this->DateThai($data[$value->vn]->vstdate);
                                                                             $data[$value->vn]->date_thai = $this->birthday_thai($data[$value->vn]->birthday);
                                                                         }else{
                                                                                             
                                                                             $data[$value->vn]->vstdate_text =  $this->DateThai($data[$value->vn]->vstdate);
                                                                            $data[$value->vn]->date_thai =  $this->birthday_thai($data[$value->vn]->birthday);
                                                                             
                                                                             
                                                                         }

                             }
                            
                          
                          }
               
                 return $data;
          }

            
     

   }
   

}
