const assert = require('assert')
const moment = require('moment-timezone')
const { toBuddhistYear } = require('./buddhist-year')

describe('ToBuddhistYear', () => {
  it('Should return corrent date and time with LLLL format', () => {
    assert.equal('Thursday, June 27, 2562 2:15 AM', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'LLLL'))
  })
  it('Should return correct date with no time with LLLL format', () => {
    assert.equal('Thursday, June 27, 2562 12:00 AM', toBuddhistYear(moment('2019-06-27'), 'LLLL'))
  })
  it('Should return corrent date and time with llll format', () => {
    assert.equal('Thu, Jun 27, 2562 2:15 AM', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'llll'))
  })
  it('Should return correct date with no time with llll format', () => {
    assert.equal('Thu, Jun 27, 2562 12:00 AM', toBuddhistYear(moment('2019-06-27'), 'llll'))
  })
  it('Should return corrent date and time with LLL format', () => {
    assert.equal('June 27, 2562 2:15 AM', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'LLL'))
  })
  it('Should return correct date with no time with LLL format', () => {
    assert.equal('June 27, 2562 12:00 AM', toBuddhistYear(moment('2019-06-27'), 'LLL'))
  })
  it('Should return corrent date and time with lll format', () => {
    assert.equal('Jun 27, 2562 2:15 AM', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'lll'))
  })
  it('Should return correct date with no time with lll format', () => {
    assert.equal('Jun 27, 2562 12:00 AM', toBuddhistYear(moment('2019-06-27'), 'lll'))
  })
  it('Should return corrent date and time with LL format', () => {
    assert.equal('June 27, 2562', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'LL'))
  })
  it('Should return correct date with no time with LL format', () => {
    assert.equal('June 27, 2562', toBuddhistYear(moment('2019-06-27'), 'LL'))
  })
  it('Should return corrent date and time with ll format', () => {
    assert.equal('Jun 27, 2562', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'll'))
  })
  it('Should return correct date with no time with ll format', () => {
    assert.equal('Jun 27, 2562', toBuddhistYear(moment('2019-06-27'), 'll'))
  })
  it('Should return corrent date and time with L format', () => {
    assert.equal('06/27/2562', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'L'))
  })
  it('Should return correct date with no time with L format', () => {
    assert.equal('06/27/2562', toBuddhistYear(moment('2019-06-27'), 'L'))
  })
  it('Should return corrent date and time with l format', () => {
    assert.equal('6/27/2562', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'l'))
  })
  it('Should return correct date with no time with l format', () => {
    assert.equal('6/27/2562', toBuddhistYear(moment('2019-06-27'), 'l'))
  })
  it('Should return corrent date and time with MM/DD/YYYY format', () => {
    assert.equal('06/27/2562', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'MM/DD/YYYY'))
  })
  it('Should return correct date with no time with MM/DD/YYYY format', () => {
    assert.equal('06/27/2562', toBuddhistYear(moment('2019-06-27'), 'MM/DD/YYYY'))
  })
  it('Should return corrent date and time with MM/DD/YY format', () => {
    assert.equal('06/27/62', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'MM/DD/YY'))
  })
  it('Should return correct date with no time with MM/DD/YY format', () => {
    assert.equal('06/27/62', toBuddhistYear(moment('2019-06-27'), 'MM/DD/YY'))
  })
  it('Should return corrent date and time with MM/DD/Y format', () => {
    assert.equal('06/27/2562', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'MM/DD/Y'))
  })
  it('Should return correct date with no time with MM/DD/Y format', () => {
    assert.equal('06/27/2562', toBuddhistYear(moment('2019-06-27'), 'MM/DD/Y'))
  })
  it('Should return correct date with no time with YYYY-MM-DDTHH:mm:ss.sssZ format', () => {
    assert.equal('2562-06-27T02:15:07.077-07:00', toBuddhistYear(moment('2019-06-27T02:15:07-07:00'), 'YYYY-MM-DDTHH:mm:ss.sssZ'))
  })
})
